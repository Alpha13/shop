import { Component } from '@angular/core';
import { CategoriesService } from './services/categories.service';

@Component({
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent {

  constructor(private categoriesService: CategoriesService) {
  }

}
