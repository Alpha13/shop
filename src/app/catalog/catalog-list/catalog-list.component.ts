import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductsService } from '../services/products.service';
import * as _ from 'lodash';
import { IProduct } from '../interfaces/iproduct';
import { CategoriesService } from '../services/categories.service';
import { ActivatedRoute } from '@angular/router';
import { ICategory } from '../interfaces/icategory';

@Component({
  selector: 'app-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss']
})
export class CatalogListComponent implements OnInit, OnDestroy {
  private sub: any;
  categoryId: number;
  products: IProduct[];
  categoriesList: ICategory[];

  constructor(private categoriesService: CategoriesService,
              private productsService: ProductsService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.categoryId = +params['id'];

      const tempCats = this.categoriesService.getCategoryById(this.categoryId);

      this.getCategories(tempCats);

      this.getProducts(
        _.map(tempCats, 'id')
      );
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getCategories(cats: ICategory[]) {
    this.categoriesList = cats.length > 1 ? cats : this.categoriesService.getCategoryById(cats[0].parentId);
  }

  getProducts(catIds: number[]) {
    this.products = this.productsService.getProducts(catIds);
  }

}
