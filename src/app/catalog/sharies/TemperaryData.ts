import { ICategory } from '../interfaces/icategory';
import { IProduct } from '../interfaces/iproduct';

export const Categories: Array<ICategory> = [
  {
    id: 1,
    name: 'Футболки',
    description: 'Некии крутые футболки уровень 1',
    parentId: 0,
    image: 'https://www.customink.com/mms/images/catalog/styles/297300/catalog_detail_image_large.jpg'
  },
  {
    id: 2,
    name: 'Футболки женские',
    description: 'Некии крутые футболки уровень 2',
    parentId: 1,
    image: 'https://cdn.pixabay.com/photo/2013/07/13/14/07/apparel-162180__340.png'
  },
  {
    id: 3,
    name: 'Футболки мужские',
    description: 'Некии крутые футболки уровень 2',
    parentId: 1,
    image: 'https://rlv.zcache.com/womens_cyclone_tie_dye_t_shirt-rb81186a0af804a1ea45ccba4531561be_jy5kj_324.jpg?square_it=true'
  },
  {
    id: 4,
    name: 'Штаны',
    description: 'Некии крутые Штаны уровень 1',
    parentId: 0,
    image: ''
  },
  {
    id: 5,
    name: 'Штаны мужские',
    description: 'Некии крутые Штаны уровень 2',
    parentId: 4,
    image: ''
  },
  {
    id: 6,
    name: 'Штаны женские',
    description: 'Некии крутые Штаны уровень 2',
    parentId: 4,
    image: ''
  },
  {
    id: 7,
    name: 'Кроссовки',
    description: 'Некии крутые Кроссовки уровень 1',
    parentId: 0,
    image: ''
  },
  {
    id: 8,
    name: 'Кроссовки женские',
    description: 'Некии крутые Кроссовки уровень 2',
    parentId: 7,
    image: ''
  },
  {
    id: 9,
    name: 'Кроссовки мужские',
    description: 'Некии крутые Кроссовки уровень 2',
    parentId: 7,
    image: ''
  }
];

export const Products: Array<IProduct> = [
  {
    id: 1,
    name: 'Крутая Мужская Футболка 1',
    description: 'Крутая Мужская Футболка',
    price: 100,
    images: [
      'https://static4.cilory.com/238653-thickbox_default/black-superman-t-shirt.jpg',
      'http://ecx.images-amazon.com/images/I/71jIRqqyfAL._UL1500_.jpg,',
      'https://d38jde2cfwaolo.cloudfront.net/226016-thickbox_default/superman-black-hoodie-t-shirt.jpg'
    ],
    categoryId: 2
  },
  {
    id: 2,
    name: 'Крутая Мужская Футболка 2',
    description: 'Крутая Мужская Футболка',
    price: 110,
    images: [
      'https://i5.walmartimages.com/asr/97b2031f-8ff8-4892-98f3-ea7b3b4124dc_1.a335594489d870271acd67cc643d6d6e.jpeg',
      'http://images.esellerpro.com/3989/I/27/j215m_frenchnavy_ft.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGdYIkpQmVPB_CPKYKbcnwVPqz7MUVboDMZ3gMn0LrADt47vHQ'
    ],
    categoryId: 2
  },
  {
    id: 2,
    name: 'Крутая Мужская Футболка 3',
    description: 'Крутая Мужская Футболка',
    price: 115,
    images: [
      'http://ep.yimg.com/ay/villagestreetwear/charlie-brown-s-t-shirt-shirt-worn-by-charlie-brown-10.jpg',
      'https://cdn.shopify.com/s/files/1/0301/0501/products/t-shirts-charlie-brown-zig-zag-2.jpg?v=1487958949',
      'https://s-media-cache-ak0.pinimg.com/736x/5b/8f/fd/5b8ffd4bcb2999f1beeefec64f32dda8.jpg'
    ],
    categoryId: 2
  },
  {
    id: 4,
    name: 'Крутая Женская Футболка 1',
    description: 'Крутая Женская Футболка',
    price: 120,
    images: [
      'https://rlv.zcache.com/womens_basic_t_shirt-r03d567e694e94ed1b27c9654fcbd63c9_k2gmm_324.jpg',
      'https://riverisland.scene7.com/is/image/RiverIsland/292027_main?$CrossSellProductPage514$',
      'https://rlv.zcache.com.pt/deslizamento_de_floydian_t_shirts-rc436d778125847eb9044dd5369c365d9_k2g55_324.jpg'
    ],
    categoryId: 3
  },
  {
    id: 5,
    name: 'Крутая Мужская Футболка 2',
    description: 'Крутая Женская Футболка',
    price: 125,
    images: [
      'https://mediastorage.soliver.com/is/image/soliver/41.705.32.4580.01D2_front?$SORL_OV$',
      'https://rlv.zcache.com/womens_bella_3_4_sleeve_raglan_t_shirt-r9d5086fdc3df4605abe6e214b1fb427a_k2gd5_324.jpg'
    ],
    categoryId: 3
  },
  {
    id: 6,
    name: 'Крутая Мужская Футболка 3',
    description: 'Крутая Женская Футболка',
    price: 130,
    images: [
      'http://tsx.in/image/cache/data/kids/tees/GIRLS/TSX-GIRLTEE-4CAT-1-600x315.jpg'
    ],
    categoryId: 3
  },

  {
    id: 7,
    name: 'Крутые Мужские Штаны 1',
    description: 'Крутые Мужские Штаны',
    price: 140,
    images: [
      'https://www.dhresource.com/600x600/f2/albu/g4/M00/98/EB/rBVaEVfpDb2AVIHDAABf5GM0HO8238.jpg'
    ],
    categoryId: 5
  },
  {
    id: 8,
    name: 'Крутые Мужские Штаны 2',
    description: 'Крутые Мужские Штаны',
    price: 145,
    images: [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaiUbSQqZPU7t1c7lw1LwV9-QAsBB7XWV__ckcOQNzwEL-8U9B'
    ],
    categoryId: 5
  },
  {
    id: 9,
    name: 'Крутые Мужские Штаны 3',
    description: 'Крутые Мужские Штаны',
    price: 150,
    images: [
      'http://style4man.com/wp-content/uploads/2014/04/%D1%81%D0%BA%D0%B8%D0%BD%D0%B8.jpg'
    ],
    categoryId: 5
  },
  {
    id: 10,
    name: 'Крутые Женские Штаны 1',
    description: 'Крутые Женские Штаны',
    price: 155,
    images: [
      'http://images.zakupka.com/i/firms/27/107/107256/uzkie-zhenskie-shtany-dudochki-internet-magazin-quot-as-torg-quot_725a1b50540a48a_200x200.jpg'
    ],
    categoryId: 6
  },
  {
    id: 11,
    name: 'Крутые Женские Штаны 2',
    description: 'Крутые Женские Штаны',
    price: 160,
    images: [
      'http://pantera.in.ua/image/cache/data/241267/794Tr6TgwD-500x612.jpg'
    ],
    categoryId: 6
  },
  {
    id: 12,
    name: 'Крутые Женские Штаны 3',
    description: 'Крутые Женские Штаны',
    price: 165,
    images: [
      'http://www.samoshvejka.ru/_nw/3/81595335.jpg'
    ],
    categoryId: 6
  },

  {
    id: 13,
    name: 'Крутые Женские Кроссовки 1',
    description: 'Крутые Женские Кроссовки',
    price: 170,
    images: [
      'http://vse-krossovki.in.ua/images/product/category/699f449.png',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQc590MGcnAkrp-nmbqtYZ1QirNPissCpx3RswgP1CFfMHGY53Q',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLxQkmA6t4qhuU7kak076dLTWk7d0X5OEWe6A3zkrSUN_n59wH'
    ],
    categoryId: 8
  },
  {
    id: 14,
    name: 'Крутые Женские Кроссовки 2',
    description: 'Крутые Женские Кроссовки',
    price: 175,
    images: [
      'http://fankyshop.net/imgs/Belo-rozovie_zhenskie_krossovki_Nike_Air_Max_90_58W_b14503.jpg',
      'http://images.ua.prom.st/187764965_w200_h200_cid1624767_pidNone-93abe849.jpg',
      'http://nazya.com/anyimage/img04.taobaocdn.com/bao/uploaded/i4/T1qF16XhXjXXXAoW_a_120219.jpg'
    ],
    categoryId: 8
  },
  {
    id: 15,
    name: 'Крутые Женские Кроссовки 3',
    description: 'Крутые Женские Кроссовки',
    price: 180,
    images: [
      'http://chevli.com.ua/image/cache/data/Nike/%D0%96%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B5%20Max%20Hyperfuse/%D0%91%D0%B8%D1%80%D1%8E%D0%B7%D0%BE%D0%B2%D1%8B%D0%B5_%D0%96%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B5_%D0%9A%D1%80%D0%BE%D1%81%D1%81%D0%BE%D0%B2%D0%BA%D0%B8_%D0%9D%D0%B0%D0%B9%D0%BA_%D0%90%D0%B8%D1%80_%D0%9C%D0%B0%D0%BA%D1%81-500x500.jpg',
      'https://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/12797887_1757489241148647_1293628682_n.jpg?ig_cache_key=MTIwMzQ4MjgwNjY4NDEzMDc1Ng%3D%3D.2.l',
      'http://pumproom.ru/image/cache/data/3434/air_max_biryuza4-800x700.jpg'
    ],
    categoryId: 8
  },
  {
    id: 16,
    name: 'Крутые Мужские Кроссовки 1',
    description: 'Крутые Мужские Кроссовки',
    price: 185,
    images: [
      'http://trainers-shop.ru/image/data/Kategorii/krossovki-mujskie-cena-nedorogo-yalta-simferopol.jpg'
    ],
    categoryId: 9
  },
  {
    id: 17,
    name: 'Крутые Мужские Кроссовки 2',
    description: 'Крутые Мужские Кроссовки',
    price: 190,
    images: [
      'http://footwear-clothes.in.ua/wp-content/uploads/Muzhskie-krossovki-Nike-Air-Force-chernye-s-belym-600x600.jpg',
      'http://footwear-clothes.in.ua/wp-content/uploads/Muzhskie-krossovki-Nike-Air-Force-chernye-s-belym-4.jpg'
    ],
    categoryId: 9
  },
  {
    id: 18,
    name: 'Крутые Мужские Кроссовки 3',
    description: 'Крутые Мужские Кроссовки',
    price: 195,
    images: [
      'https://shoessale.com.ua/360/4/1/7/41797/thumb_41797_0.jpg',
      'https://shoessale.com.ua/public/360/4/2/9/42952/42952_0.jpg',
      'http://www.твоя-обувь.com.ua/image/cache/630-552/data/2153/yzy-boost-2153.jpg'
    ],
    categoryId: 9
  }
];
