import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CatalogComponent } from './catalog.component';
import { CatalogCategoriesComponent } from './catalog-categories/catalog-categories.component'
import { CatalogListComponent } from './catalog-list/catalog-list.component';
import { ProductComponent } from './product/product.component';

const catalogRoutes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      {
        path: '',
        data: {
          breadcrumb: 'Categories'
        },
        component: CatalogCategoriesComponent
      },
      {
        path: 'c/:id',
        data: {
          breadcrumb: 'Category\'s page'
        },
        component: CatalogListComponent
      },
      {
        path: 'p/:id',
        data: {
          breadcrumb: 'Product page'
        },
        component: ProductComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(catalogRoutes)],
  exports: [RouterModule]
})

export class CatalogRoutingModule {
}

