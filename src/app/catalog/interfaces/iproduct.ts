export interface IProduct {
  id: number;
  name: string;
  description?: string;
  price: number;
  priceOrigin?: number;
  images?: string[];
  categoryId: number;
}
