export interface IBasketItem {
  id: number;
  price: number;
  priceOrigin?: number;
  name: string;
  amount: number;
}
