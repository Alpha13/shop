import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Services
import { CategoriesService } from './services/categories.service';
import { ProductsService } from './services/products.service';
import { BasketService } from './services/basket.service';

import { CatalogComponent } from './catalog.component';
import { CatalogListComponent } from './catalog-list/catalog-list.component';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogCategoriesComponent } from './catalog-categories/catalog-categories.component';
import { ProductComponent } from './product/product.component';

@NgModule({
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule
  ],
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogCategoriesComponent,
    ProductComponent
  ],
  providers: [
    CategoriesService,
    ProductsService,
    BasketService
  ]
})

export class CatalogModule {
}
