import { Component, OnInit } from '@angular/core';
import { ICategory } from '../interfaces/icategory';
import { CategoriesService } from '../services/categories.service';

@Component({
  selector: 'app-catalog-categories',
  templateUrl: './catalog-categories.component.html',
  styleUrls: ['./catalog-categories.component.scss']
})
export class CatalogCategoriesComponent implements OnInit {

  categoriesList: ICategory[];

  constructor(private categoriesService: CategoriesService) {
    this.categoriesList = categoriesService.getCategoryById(0);
  }

  ngOnInit() {

  }

}
