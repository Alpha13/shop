import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from '../interfaces/iproduct';
import { BasketService } from '../services/basket.service';

@Component({
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  private sub: any;
  productId: number;
  product: IProduct;
  buyingAmount = 0;

  constructor(private route: ActivatedRoute,
              private productsService: ProductsService,
              private basketService: BasketService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.productId = +params['id'];
    });

    this.getProduct(this.productId);
  }

  getProduct(id: number) {
    this.product = this.productsService.getProductById(this.productId);
  }

  onBuying() {
    console.log('buyingAmount : ', this.buyingAmount);

    this.basketService.add({
      id: this.product.id,
      name: this.product.name,
      price: this.product.price,
      amount: this.buyingAmount
    });


  }

}
