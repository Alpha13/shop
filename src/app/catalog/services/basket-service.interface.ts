import { EventEmitter } from '@angular/core';

export interface IBasketItem {
  id: number;
  name: string;
  amount: number;
  price: number;
}

export interface IBasketService {

  /**
   * Example: localStorage, sessionStorage
   */
  storage: any;

  /**
   * Events
   */
  onUpdate: EventEmitter<IBasketItem[]>;

  onAdd: EventEmitter<IBasketItem>;

  onRemove: EventEmitter<IBasketItem|any>;

  onClean: EventEmitter<any>;

  /**
   * Items of basket
   */
  items: IBasketItem[];

  /**
   * Basket is empty
   */
  isEmpty(): boolean;

  /**
   * Clear basket from storage
   */
  clear(): void;

  /**
   * Add item to basket
   */
  add(item: IBasketItem): void;

  /**
   * Remove item from basket
   */
  remove(item: IBasketItem): void;

  /**
   * Update all items
   * @param items
   */
  update(items: IBasketItem[]): void;

  getTotalSum(): number;

  getTotalCount(): number;
}
