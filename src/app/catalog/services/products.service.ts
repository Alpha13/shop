import { Injectable } from '@angular/core';
import { IProduct } from '../interfaces/iproduct';
import { Products } from '../sharies/TemperaryData';
import * as _ from 'lodash';

@Injectable()
export class ProductsService {

  products: IProduct[];

  constructor() {
    this.products = Products;
  }

  getProducts(ids: number[]): IProduct[] {
    return _.filter(this.products, function (p) {
      return _.indexOf(ids, p.categoryId) > -1;
    });
  }

  getProductById(id: number): IProduct {
    return _.find(this.products, {'id': id});
  }

}
