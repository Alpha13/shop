import { Injectable } from '@angular/core';
import { ICategory } from '../interfaces/icategory';
import { Categories } from '../sharies/TemperaryData';
import * as _ from 'lodash';

@Injectable()
export class CategoriesService {
  categories: ICategory[];

  constructor() {
    this.categories = Categories;
  }

  getCategories(id = 0): ICategory[] {
    console.log(createCategory(id));
    return createCategory(id);
  }

  getCategoryById(id: number): ICategory[] {
    return _.filter(this.categories, function (c) {
      return (c.id === id || c.parentId === id);
    });
  }


}

function createCategory(parentId: number) {
  const list = _.filter(Categories, function (c) {
    return c.parent === parentId;
  });
  return _.forEach(list, function (c) {
    c.children = createCategory(c.id);
  });
}
