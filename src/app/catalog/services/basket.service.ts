import { EventEmitter, Injectable } from '@angular/core';
import { IBasketItem, IBasketService } from './basket-service.interface';

@Injectable()
export class BasketService implements IBasketService {
  static STORAGE_KEY = 'basketItems';
  storage: any;

  onUpdate: EventEmitter<IBasketItem[]> = new EventEmitter<IBasketItem[]>();
  onAdd: EventEmitter<IBasketItem> = new EventEmitter<IBasketItem>();
  onRemove: EventEmitter<IBasketItem> = new EventEmitter<IBasketItem>();
  onClean: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    this.storage = localStorage;

    if (!this.items) {
      this.items = [];
    }
  }

  isEmpty(): boolean {
    return !this.items || !this.items.length;
  }

  clear(): void {
    this.items = [];
    this.onClean.emit();
  }

  add(item: IBasketItem): void {
    this.items.push(item);
    this.onAdd.emit(item);
  }

  remove(item: IBasketItem): void {
    const id = this.items.indexOf(item);
    if (id !== -1) {
      this.items.slice(id, 1);
      this.onRemove.emit(item);
    }
  }

  update(items: IBasketItem[]): void {
    this.onUpdate.emit(items);
  }

  getTotalSum(): number {
    let sum = 0;
    this.items.forEach(i => sum += i.amount * i.price);
    return sum;
  }

  getTotalCount(): number {
    let amount = 0;
    this.items.forEach(i => amount += i.amount);
    return amount;
  }

  set items(items: IBasketItem[]) {
    this.storage.setItem(BasketService.STORAGE_KEY, JSON.stringify(items));
  }

  get items(): IBasketItem[] {
    return JSON.parse(this.storage.getItem(BasketService.STORAGE_KEY));
  }

}
