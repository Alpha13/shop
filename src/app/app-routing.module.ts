import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    data: {
      breadcrumb: 'Home'
    },
    component: HomePageComponent
  },
  {
    path: 'catalog',
    data: {
      breadcrumb: 'Catalog'
    },
    loadChildren: 'app/catalog/catalog.module#CatalogModule'
  },
  {
    path: '**',
    data: {
      breadcrumb: 'Wrong Stage'
    },
    component: NotFoundPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: []
})

export class AppRoutingModule {
}
